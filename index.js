/*

1. What directive is used by Node.js in loading the modules it needs?

	- require("http");

2. What Node.js module contains a method for server creation?

	- http

3. What is the method of the http object responsible for creating a server using Node.js?

	- createServer()

4. What method of the response object allows us to set status codes and content types?

	- writeHead()

5. Where will console.log() output its contents when run in Node.js?

	- terminal

6. What property of the request object contains the address's endpoint?

	- url

*/


const http = require("http");

const port = 3000;

const server = http.createServer(function(req, res) {

	if(req.url =="/login"){

	res.writeHead(200, {'Content-Type': 'text/plain'});

	res.end("Welcome to the login page.");
	} else if(req.url == "/register") {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end("I'm sorry the page you are looking for cannot be found");
	} else {

		res.writeHead(404, {'Content-Type': 'text/plain'});

		res.end("Page Not Found");
	} 

});

server.listen(port);

console.log(`Server is now accessible at loaclhost:${port}`);
















